#!/usr/bin/env bash
# ###########################################	#
# Qt and PyQt [5.14.2] compile from source 	#
# Tested in Raspberry pi 3 B+ [stretch]	#
# SD card size: 16GB				#
# 18-08-2021					#
# ###########################################	#

echo -e "\e[92m=== Free up space ==="
cd /home/pi/
sudo apt-get clean
sudo apt-get autoremove -y
sudo apt-get clean
sudo rm -rf /home/pi/.local/share/Trash/*
sudo swapoff /myswap
sudo rm /myswap

echo -e "\e[92m=== Editing sources ==="
sudo rm /etc/apt/sources.list
cat >> sources.list <<- "EOF"
deb http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi
deb-src http://raspbian.raspberrypi.org/raspbian/ stretch main contrib non-free rpi
EOF
sudo mv ./sources.list /etc/apt/sources.list

echo -e "\e[92m=== Enabling 1GB swap ==="
free -m
sudo dd if=/dev/zero of=/myswap bs=1024 count=1000k status=progress
sudo mkswap /myswap
sudo swapon /myswap
free -m

echo -e "\e[92m=== Installing Qt dependences ==="
cd /home/pi/
sudo apt-get update
sudo pip3 install -I --upgrade --force-reinstall setuptools
sudo apt-get update
sudo apt-get --assume-yes build-dep qt4-x11
sudo apt-get --assume-yes build-dep libqt5gui5
sudo apt-get --assume-yes build-dep qtbase-opensource-src
sudo apt-get --assume-yes install libudev-dev libinput-dev libts-dev libxcb-xinerama0-dev libxcb-xinerama0
sudo apt-get --assume-yes install cmake libpq-dev qt5-default libqt5svg5-dev libqt5sql5-psql libqt5sql5-mysql libpq-dev libpng-dev build-essential comerr-dev libaudio-dev libaudio2 libc6-dev libexpat1-dev libfontconfig1-dev libfreetype6-dev libgl1-mesa-dev libglib2.0-dev libglu1-mesa-dev libice-dev libkrb5-dev libpq-dev libpq5 libsm-dev libsqlite0-dev libssl-dev libx11-dev libxau-dev libxcb1-dev libxcursor-dev libxdmcp-dev libxext-dev libxfixes-dev libxft-dev libxi-dev libxinerama-dev libxmu-dev libxmu-headers libxrandr-dev libxrender-dev libxt-dev linux-libc-dev mesa-common-dev mysql-common x11proto-core-dev x11proto-fixes-dev x11proto-input-dev x11proto-kb-dev x11proto-randr-dev x11proto-render-dev x11proto-xext-dev x11proto-xinerama-dev xtrans-dev zlib1g-dev libmysqlcppconn-dev libcups2-dev libcupsimage2-dev libtiff-dev libjpeg-dev libjpeg62-turbo-dev libxcb-xinerama0 libxcb-xinerama0-dev libqt5gstreamer-dev libxcb1 libxcb1-dev libx11-xcb1 libx11-xcb-dev libxcb-keysyms1 libxcb-keysyms1-dev libxcb-image0 libxcb-image0-dev libxcb-shm0 libxcb-shm0-dev libxcb-icccm4 libxcb-icccm4-dev libxcb-sync-dev libxcb-xfixes0-dev libxrender-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0 libxcb-render-util0-dev libxcb-glx0-dev libasound2-dev libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libqt5sql5-ibase libqt5multimedia5 qtmultimedia5-dev libqt5multimedia5-plugins libxkbcommon-dev libxkbcommon-x11-0 libxkbcommon-x11-dev libxkbcommon0 libudev1 udev
sudo apt-get --assume-yes install libfontconfig1-dev libfreetype6-dev libx11-dev libxext-dev libxfixes-dev libxi-dev libxrender-dev libxcb1-dev libx11-xcb-dev libxcb-glx0-dev libxcb-keysyms1-dev libxcb-image0-dev libxcb-shm0-dev libxcb-icccm4-dev libxcb-sync0-dev libxcb-xfixes0-dev libxcb-shape0-dev libxcb-randr0-dev libxcb-render-util0-dev
sudo apt-get --assume-yes install build-essential git libfontconfig1-dev libdbus-1-dev libfreetype6-dev libicu-dev libinput-dev libxkbcommon-dev libsqlite3-dev libssl-dev libpng-dev libjpeg-dev libglib2.0-dev libraspberrypi-dev libcups2-dev libasound2-dev

echo -e "\e[92m=== Downloading and extracting source ==="
cd /home/pi/
wget https://download.qt.io/archive/qt/5.14/5.14.2/single/qt-everywhere-src-5.14.2.tar.xz
sudo tar -xf qt-everywhere-src-5.14.2.tar.xz
sudo rm qt-everywhere-src-5.14.2.tar.xz  # tar.xz is removed to free up space

echo -e "\e[92m=== Building source in /home/pi/qt-build ==="
cd /home/pi/
sudo mkdir /home/pi/qt-build
sudo chmod -R 777 /home/pi/qt-build
cd /home/pi/qt-build
sudo /home/pi/qt-everywhere-src-5.14.2/configure \
	-v \
	-recheck-all -optimize-size \
	-device linux-rasp-pi-g++ \
	-device-option CROSS_COMPILE=/usr/bin/ \
	-opengl es2 -eglfs \
	-no-gtk \
	-opensource -confirm-license -release \
	-reduce-exports \
	-nomake examples -no-compile-examples \
	-nomake tools -nomake tests \
	-qt-pcre \
	-no-pch \
	-ssl \
	-evdev \
	-system-freetype \
	-fontconfig \
	-prefix /usr/local/qt5 \
	-qpa eglfs \
	-skip qt3d \
	-skip qtactiveqt \
	-skip qtandroidextras \
	-skip qtcharts \
	-skip qtdatavis3d \
	-skip qtdeclarative \
	-skip qtdoc \
	-skip qtgamepad \
	-skip qtgraphicaleffects \
	-skip qtimageformats \
	-skip qtlocation \
	-skip qtlottie \
	-skip qtmacextras \
	-skip qtmultimedia \
	-skip qtnetworkauth \
	-skip qtpurchasing \
	-skip qtquick3d \
	-skip qtquickcontrols \
	-skip qtquickcontrols2 \
	-skip qtquicktimeline \
	-skip qtremoteobjects \
	-skip qtscript \
	-skip qtscxml \
	-skip qtsensors \
	-skip qtserialbus \
	-skip qtserialport \
	-skip qtspeech \
	-skip qtsvg \
	-skip qttools \
	-skip qttranslations \
	-skip qtvirtualkeyboard \
	-skip qtwayland \
	-skip qtwebchannel \
	-skip qtwebengine \
	-skip qtwebglplugin \
	-skip qtwebsockets \
	-skip qtwebview \
	-skip qtwinextras \
	-skip qtx11extras \
	-skip qtxmlpatterns \
	|& sudo tee -a /home/pi/qt-build/qt_configure.log

sudo make -j2 |& sudo tee -a /home/pi/qt-build/qt_make.log
sudo make install |& sudo tee -a /home/pi/qt-build/qt_make_ins.log

echo -e "\e[92m=== Linking qt ==="
sudo mv /usr/lib/arm-linux-gnueabihf/qt5 /usr/lib/arm-linux-gnueabihf/qt5_old
sudo ln -s -f /usr/local/qt5/ /usr/lib/arm-linux-gnueabihf/qt5

sudo mv /usr/share/qt5 /usr/share/qt5_old
sudo ln -s -f /usr/local/qt5/ /usr/share/qt5

cd /home/pi/
sudo rm /usr/share/qtchooser/qt5.conf
cat >> qt5.conf <<- "EOF"
/usr/local/qt5/bin
/usr/local/qt5/lib
EOF
sudo mv  /home/pi/qt5.conf /usr/share/qtchooser/qt5.conf
sudo ln -s -f /usr/share/qtchooser/qt5.conf /usr/lib/arm-linux-gnueabihf/qtchooser/qt5.conf
sudo ln -s -f /usr/share/qtchooser/qt5.conf /usr/lib/arm-linux-gnueabihf/qt-default/qtchooser/default.conf

QT_SELECT=qt5
sudo qtchooser -print-env
sudo qmake -v
sudo qmake -query

rm -rf /home/pi/.local/share/Trash/*

echo -e "\e[92m=== Removing old PyQt5 ==="
cd /home/pi/
sudo pip3 uninstall pyqt5
sudo apt-get --assume-yes remove python3-pyqt5* || True
sudo mv /usr/lib/python3/dist-packages/PyQt5 /usr/lib/python3/dist-packages/PyQt5_old
sudo mv /usr/bin/lsb_release /usr/bin/lsb_release_back
sudo rm -r PyQt5-5.14.2.tar.gz
sudo rm -r /usr/lib/python3.5/PyQt5
sudo rm -r /usr/lib/python3.5/PyQt5-5.14.2.dist-info
sudo rm -r /usr/lib/python3/dist-packages/PyQt5

echo -e "\e[92m=== Installing PyQt5 requirements ==="
sudo pip3 install sip
sudo apt-get --assume-yes install libgl1-mesa-glx-lts-utopic
sudo pip3 install PyQt5-sip
sudo pip3 install pyqt-builder

sudo mkdir /usr/lib/python3/dist-packages/PyQt5
sudo ln -s -f /home/pi/.local/lib/python3.5/site-packages/PyQt5/sip.cpython-35m-arm-linux-gnueabihf.so /usr/lib/python3/dist-packages/PyQt5

echo -e "\e[92m=== Installing PyQt5 ==="
cd /home/pi/
wget https://files.pythonhosted.org/packages/4d/81/b9a66a28fb9a7bbeb60e266f06ebc4703e7e42b99e3609bf1b58ddd232b9/PyQt5-5.14.2.tar.gz
sudo tar -xf PyQt5-5.14.2.tar.gz
sudo rm PyQt5-5.14.2.tar.gz
cd /home/pi/PyQt5-5.14.2
sudo sip-install --verbose --no-designer-plugin --confirm-license --no-tools \
	|& sudo tee -a /home/pi/qt-build/sip_install.log

# Created on python3.5?
sudo ln -s -f /usr/local/lib/python3.5/dist-packages/PyQt5/sip.cpython-35m-arm-linux-gnueabihf.so /usr/lib/python3.5/PyQt5/
sudo ln -s -f /usr/local/lib/python3.5/dist-packages/PyQt5_sip-12.7.2.dist-info /usr/lib/python3.5/

# Created on python3?
sudo ln -s -f /usr/local/lib/python3.5/dist-packages/PyQt5/sip.cpython-35m-arm-linux-gnueabihf.so /usr/lib/python3/dist-packages/PyQt5/
sudo ln -s -f /usr/local/lib/python3.5/dist-packages/PyQt5_sip-12.7.2.dist-info /usr/lib/python3/

echo -e "\e[92m=== Linking correct broadcom libs ==="
sudo mv /usr/lib/arm-linux-gnueabihf/libGLESv2.so.2.0.0 libGLESv2.so.2.0.0_old
sudo ln -s -f /opt/vc/lib/libGLESv2.so /usr/lib/arm-linux-gnueabihf/libGLESv2.so.2.0.0
sudo ln -s -f /opt/vc/lib/libbrcmGLESv2.so /opt/vc/lib/libGLESv2.so

sudo mv /usr/lib/arm-linux-gnueabihf/libEGL.so.1.0.0 libEGL.so.1.0.0_old
sudo ln -s -f /opt/vc/lib/libEGL.so /usr/lib/arm-linux-gnueabihf/libEGL.so.1.0.0
sudo ln -s -f /opt/vc/lib/libbrcmEGL.so /opt/vc/lib/libEGL.so

echo -e "\e[92m=== Disabling swap ==="
sudo swapoff /myswap
sudo rm /myswap

echo -e "\e[92m=== Project Building finished ==="
echo -e "\e[92mQt was built in /home/pi/qt-build"
echo -e "\e[92mQt was installed in /usr/local/qt5"
echo -e "\e[92mPyQt was installed in /usr/lib/python3/dist-packages/PyQt5"
